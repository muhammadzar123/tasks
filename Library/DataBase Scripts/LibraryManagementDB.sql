Create Database LibraryDB


CREATE TABLE LibraryDB.dbo.BooksCategories (
    CategoryId int IDENTITY(1,2) PRIMARY KEY,
    CategoryTitle VARCHAR(500) NOT NULL,
    CategoryStatus bit default 1 NOT NULL
);
CREATE TABLE LibraryDB.dbo.Authors (
    AuthorId int IDENTITY(1,2) PRIMARY KEY,
    FirstName VARCHAR(500) NOT NULL,
    LastNmme VARCHAR(500) NOT NULL
);

CREATE TABLE LibraryDB.dbo.Books (
    BookId int IDENTITY(1,2) PRIMARY KEY,
    BookTitle VARCHAR(max) NOT NULL,
    BookIsbn VARCHAR(13) NOT NULL,
    BookPublicationDate DATE NOT NULL,
    BookPublisher VARCHAR(255) NOT NULL,
    BookLanguage VARCHAR(50) NOT NULL,
	BookStatus  bit default 1 not null,
	BookStock bit default 1 not null,
	FK_AutherId_Book int,
	FK_CategoryId_Book int

	CONSTRAINT FK_AutherId_Book FOREIGN KEY (FK_AutherId_Book)
    REFERENCES Authors(AuthorId),

	CONSTRAINT FK_CategoryId_Book FOREIGN KEY (FK_CategoryId_Book)
    REFERENCES BooksCategories(CategoryId),
);

CREATE TABLE LibraryDB.dbo.BooksSubCategories (

	SubCategoryId int IDENTITY(1,2) PRIMARY KEY,
    SubCategoryTitle VARCHAR(500) NOT NULL,
    SubCategoryStatus bit default 1 NOT NULL,
	FK_CategoryId int 

	CONSTRAINT FK_CategoryId FOREIGN KEY (FK_CategoryId)
    REFERENCES BooksCategories(CategoryId),
);