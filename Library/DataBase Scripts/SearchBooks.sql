/****** Object:  StoredProcedure [dbo].[sp_GetAllGuestCustomers]    Script Date: 3/31/2023 3:10:09 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO




CREATE PROCEDURE [dbo].[sp_SearchBooks] 

 @SearchKey nvarchar(50)
  AS

-- Start of Query

;With 
Books as
(
	select * from LibraryDB.dbo.Books
	where BookStatus = 1
)
Select
BookId,
    BookTitle,
    BookIsbn,
    BookPublicationDate,
    BookPublisher,
    BookLanguage,
	BookStatus ,
	BookStock,
	FK_AutherId_Book,
	FK_CategoryId_Book

  from LibraryDB.dbo.Books
  where ( BookTitle like '%'+@SearchKey+'%' or BookPublisher like '%'+@SearchKey+'%')  order by BookPublicationDate
;
;
GO