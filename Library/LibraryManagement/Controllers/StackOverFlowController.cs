﻿using LibraryManagement.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace LibraryManagement.Controllers
{
    public class StackOverFlowController : ApiController
    {
        private const string baseUrl = "https://api.stackexchange.com/2.3";

        [AllowAnonymous]
        [AcceptVerbs("GET", "POST", "PUT")]
        [Route("api/StackOverFlow/getLastQuestions")]
        public async Task<IHttpActionResult> GetLastQuestions()
        {
            // Call the Stack Overflow API to get the last 50 questions
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync($"{baseUrl}/questions?order=desc&sort=activity&site=stackoverflow&pagesize=50");

                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<QuestionsResponse>(json);

                    if (data.Items.Count() <= 0)
                    {
                        return Json(new
                        {
                            Info = new
                            {
                                Status = HttpStatusCode.NotFound,
                                Message = "Not Found."
                            }
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            QuestionsResponse = data.Items,
                            Info = new
                            {
                                Status = HttpStatusCode.OK
                            }
                        });
                    }
                }
                return Json(new
                {
                    Info = new
                    {
                        Status = HttpStatusCode.BadRequest,
                        Message = "Not Accessable."
                    }
                });
            }
        }


        [AcceptVerbs("GET", "POST", "PUT")]
        [Route("api/StackOverFlow/getSpecificQuestion/{id}")]
        public async Task<IHttpActionResult> GetSpecificQuestion(int id)
        {
            // Call the Stack Overflow API to get the details of a specific question
            using (var client = new HttpClient())
            {
                var response = await client.GetAsync($"{baseUrl}/questions/{id}?order=desc&sort=activity&site=stackoverflow");
                if (response.IsSuccessStatusCode)
                {
                    var json = await response.Content.ReadAsStringAsync();
                    var data = JsonConvert.DeserializeObject<QuestionsResponse>(json);
                }
            }
            return NotFound();
        }
    }
}