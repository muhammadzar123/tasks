﻿using LibraryDataAccess;
using LibraryManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace LibraryManagement.Controllers
{
    [Authorize]
    public class BookController : ApiController
    {
        // GET: api/Book/getAllBooks
        [AllowAnonymous]
        [AcceptVerbs("GET", "POST", "PUT")]
        [Route("api/Book/getAllBooks")]
        public IHttpActionResult GetBooks()
        {
            try
            {
                using (LibraryDBEntities entities = new LibraryDBEntities())
                {
                    var booksData = entities.Books.Where(e => e.BookStatus == true).ToList();
                    if(booksData.Count() > 0)
                    {
                        return Json(new
                        {
                            Books = booksData,
                            Info = new
                            {
                                Status = HttpStatusCode.OK
                            }
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            Info = new
                            {
                                Status = HttpStatusCode.NotFound,
                                Message = "No Books Found!"
                            }
                        });
                    }
                    
                }
            }
            catch (Exception e)
            {
                
                return Json(new
                {
                    Info = new
                    {
                        Status = HttpStatusCode.BadRequest,
                        Message = e.Message
                    }
                });
            }
        }

        // GET: api/Book/getBook/destiny
        [AllowAnonymous]
        [AcceptVerbs("GET", "POST", "PUT")]
        [Route("api/Book/getBook/{getBookByTitle}")]
        public IHttpActionResult GetBookByTitle(string bookTitle)
        {
            try
            {
                using (LibraryDBEntities entities = new LibraryDBEntities())
                {
                    var booksData = entities.Books.Where(e => e.BookStatus == true && e.BookTitle.ToLower() == bookTitle.ToLower()).FirstOrDefault();
                    if (booksData != null)
                    {
                        return Json(new
                        {
                            Books = booksData,
                            Info = new
                            {
                                Status = HttpStatusCode.OK
                            }
                        });
                    }
                    else
                    {
                        return Json(new
                        {
                            Info = new
                            {
                                Status = HttpStatusCode.NotFound,
                                Message = $"No Book Found with the Title {bookTitle}!"
                            }
                        });
                    }

                }
            }
            catch (Exception e)
            {

                return (Json(new
                {
                    Info = new
                    {
                        Status = HttpStatusCode.BadRequest,
                        Message = e.Message
                    }
                }));
            }
        }

        // POST: api/Book/AddBook
        [AllowAnonymous]
        [AcceptVerbs("GET", "POST", "PUT")]
        [Route("api/Book/addBook")]
        public async Task<IHttpActionResult> AddBook(AddBookModel model)
        {
            try
            {
                if (model == null)
                {
                    return Json(new
                    {
                        Info = new
                        {
                            Status = HttpStatusCode.BadRequest,
                            Message = "Please put all required fields."
                        }
                    });
                }
                if (!ModelState.IsValid)
                {
                    return Json(new
                    {
                        Info = new
                        {
                            Status = HttpStatusCode.BadRequest,
                            Message = "Please put all required fields.",
                            DetailMessage = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)),
                            //StackTrace = ModelState
                        }
                    });
                }
                using (LibraryDBEntities entities = new LibraryDBEntities())
                {
                    var booksData = entities.Books.Where(e => e.BookStatus == true && e.BookTitle.ToLower() == model.BookTitle.ToLower()).FirstOrDefault();
                    if (booksData != null)
                    {
                        return Json(new
                        {
                            Info = new
                            {
                                Status = HttpStatusCode.BadRequest,
                                Message = $"Book with this Title is already added {model.BookTitle}!"
                            }
                        });
                    }
                    Book book = new Book();
                    book.BookTitle = model.BookTitle;
                    book.BookIsbn = model.BookIsbn;
                    book.BookPublicationDate = model.BookPublicationDate;
                    book.BookPublisher = model.FK_AutherId_Book.HasValue && entities.Authors.Where(e => e.AuthorId == model.FK_AutherId_Book.Value).FirstOrDefault() != null ? entities.Authors.Where(e=>e.AuthorId == model.FK_AutherId_Book.Value).Select(e=>(e.FirstName + e.LastNmme)).FirstOrDefault() : model.BookPublisher;
                    book.BookLanguage = model.BookLanguage;
                    book.BookStatus = true;
                    book.BookStock = model.BookStock;
                    book.FK_AutherId_Book = model.FK_AutherId_Book.HasValue && entities.Authors.Where(e => e.AuthorId == model.FK_AutherId_Book.Value).FirstOrDefault() != null ? model.FK_AutherId_Book : entities.Authors.Where(e => e.FirstName.ToLower() == "unknown").Select(e=>e.AuthorId).FirstOrDefault();
                    book.FK_CategoryId_Book = model.FK_CategoryId_Book.HasValue && entities.BooksCategories.Where(e => e.CategoryId == model.FK_CategoryId_Book.Value).FirstOrDefault() != null ? model.FK_CategoryId_Book : entities.BooksCategories.Where(e => e.CategoryTitle.ToLower() == "uncategorised").Select(e=>e.CategoryId).FirstOrDefault();

                    entities.Books.Add(book);
                    await entities.SaveChangesAsync();
                    return Json(new
                    {
                        Info = new
                        {
                            Status = HttpStatusCode.OK,
                            Message = $"Successfully Added!"
                        }
                    });


                }
            }
            catch (Exception e)
            {

                return Json(new
                {
                    Info = new
                    {
                        Status = HttpStatusCode.BadRequest,
                        Message = e.Message
                    }
                });
            }
        }

        // PUT: api/Book/updateBook
        [AllowAnonymous]
        [AcceptVerbs("GET", "POST", "PUT")]
        [Route("api/Book/updateBook")]
        public async Task<IHttpActionResult> UpdateBook(AddBookModel model)
        {
            try
            {
                if (model == null)
                {
                    return Json(new
                    {
                        Info = new
                        {
                            Status = HttpStatusCode.BadRequest,
                            Message = "Please put all required fields."
                        }
                    });
                }
                if (!ModelState.IsValid)
                {
                    return Json(new
                    {
                        Info = new
                        {
                            Status = HttpStatusCode.BadRequest,
                            Message = "Please put all required fields.",
                            DetailMessage = string.Join(" | ", ModelState.Values.SelectMany(v => v.Errors).Select(e => e.ErrorMessage)),
                            //StackTrace = ModelState
                        }
                    });
                }
                using (LibraryDBEntities entities = new LibraryDBEntities())
                {
                    var book = entities.Books.Where(e => e.BookStatus == true && e.BookId == model.BookId).FirstOrDefault();
                    if (book == null)
                    {
                        return Json(new
                        {
                            Info = new
                            {
                                Status = HttpStatusCode.BadRequest,
                                Message = $"No Book Found with this Id {model.BookId}!"
                            }
                        });
                    }
                    book.BookTitle = model.BookTitle;
                    book.BookIsbn = model.BookIsbn;
                    book.BookPublicationDate = model.BookPublicationDate;
                    book.BookPublisher = model.FK_AutherId_Book.HasValue && entities.Authors.Where(e => e.AuthorId == model.FK_AutherId_Book.Value).FirstOrDefault() != null ? entities.Authors.Where(e => e.AuthorId == model.FK_AutherId_Book.Value).Select(e => (e.FirstName + e.LastNmme)).FirstOrDefault() : model.BookPublisher;
                    book.BookLanguage = model.BookLanguage;
                    book.BookStatus = model.BookStatus;
                    book.BookStock = model.BookStock;
                    book.FK_AutherId_Book = model.FK_AutherId_Book.HasValue && entities.Authors.Where(e => e.AuthorId == model.FK_AutherId_Book.Value).FirstOrDefault() != null ? model.FK_AutherId_Book : entities.Authors.Where(e => e.FirstName.ToLower() == "unknown").Select(e => e.AuthorId).FirstOrDefault();
                    book.FK_CategoryId_Book = model.FK_CategoryId_Book.HasValue && entities.BooksCategories.Where(e => e.CategoryId == model.FK_CategoryId_Book.Value).FirstOrDefault() != null ? model.FK_CategoryId_Book : entities.BooksCategories.Where(e => e.CategoryTitle.ToLower() == "uncategorised").Select(e => e.CategoryId).FirstOrDefault();

                    await entities.SaveChangesAsync();
                    return Json(new
                    {
                        Info = new
                        {
                            Status = HttpStatusCode.OK,
                            Message = $"Successfully Updated!"
                        }
                    });
                }
            }
            catch (Exception e)
            {

                return Json(new
                {
                    Info = new
                    {
                        Status = HttpStatusCode.BadRequest,
                        Message = e.Message
                    }
                });
            }
        }

        // DELETE: api/Book/DeleteBook/5
        [AllowAnonymous]
        [AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [Route("api/Book/DeleteBook/{id}")]
        public async Task<IHttpActionResult> DeleteBook(int id)
        {
            try
            {
                using (LibraryDBEntities entities = new LibraryDBEntities())
                {
                    var book = entities.Books.Where(e => e.BookStatus == true && e.BookId == id).FirstOrDefault();
                    if (book == null)
                    {
                        return Json(new
                        {
                            Info = new
                            {
                                Status = HttpStatusCode.BadRequest,
                                Message = $"No Book Found with this Id {id}!"
                            }
                        });
                    }
                    entities.Books.Remove(book);
                    await entities.SaveChangesAsync();
                    return Json(new
                    {
                        Info = new
                        {
                            Status = HttpStatusCode.OK,
                            Message = $"Successfully Deleted!"
                        }
                    });
                }
            }
            catch (Exception e)
            {

                return Json(new
                {
                    Info = new
                    {
                        Status = HttpStatusCode.BadRequest,
                        Message = e.Message
                    }
                });
            }
        }

        //DELETE: api/Book/SearchBook/des
        [AllowAnonymous] //Stored Procedure
        [AcceptVerbs("GET", "POST", "PUT", "DELETE")]
        [Route("api/Book/SearchBook/{searchKey}")]
        public IHttpActionResult SearchBook(string searchKey)
        {
            try
            {
                using (LibraryDBEntities entities = new LibraryDBEntities())
                {
                    var book = entities.sp_SearchBooks(searchKey).ToList();
                    if (book.Count() <= 0)
                    {
                        return Json(new
                        {
                            Info = new
                            {
                                Status = HttpStatusCode.BadRequest,
                                Message = $"No Books Found with this SearchKey: {searchKey}!"
                            }
                        });
                    }
                    var finalData = (from result in book
                                     select new
                                     {
                                         result.BookId,
                                         result.BookTitle,
                                         result.BookIsbn,
                                         result.BookPublicationDate,
                                         result.BookPublisher,
                                         result.BookLanguage,
                                         result.BookStatus,
                                         result.BookStock,
                                         result.FK_AutherId_Book,
                                         result.FK_CategoryId_Book
                                     }).ToList();
                    
                    return Json(new
                    {
                        SearchData = finalData,
                        Info = new
                        {
                            Status = HttpStatusCode.OK
                        }
                    });
                }
            }
            catch (Exception e)
            {

                return Json(new
                {
                    Info = new
                    {
                        Status = HttpStatusCode.BadRequest,
                        Message = e.Message
                    }
                });
            }
        }
    }
}