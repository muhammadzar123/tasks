﻿using Newtonsoft.Json;
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace LibraryManagement.Models
{
    public class AddBookModel
    {
        [Required(ErrorMessage = "Brand Id is missing.")]
        [JsonProperty("BookId", Required = Required.Default)]
        public int BookId { get; set; }
        
        [JsonProperty("BookTitle", Required = Required.Always)]
        public string BookTitle { get; set; }
        
        [JsonProperty("BookIsbn", Required = Required.Default)]
        public string BookIsbn { get; set; }
        
        [JsonProperty("BookPublicationDate", Required = Required.Default)]
        public DateTime BookPublicationDate { get; set; }
        
        [JsonProperty("BookPublisher", Required = Required.Default)]
        public string BookPublisher { get; set; }
        
        [JsonProperty("BookLanguage", Required = Required.Default)]
        public string BookLanguage { get; set; }
        
        [JsonProperty("BookStatus", Required = Required.Default)]
        public bool BookStatus { get; set; }
        
        [JsonProperty("BookTitle", Required = Required.Default)]
        public bool BookStock { get; set; }
        
        [JsonProperty("FK_AutherId_Book", Required = Required.Default)]
        public int? FK_AutherId_Book { get; set; }
        
        [JsonProperty("FK_CategoryId_Book", Required = Required.Default)]
        public int? FK_CategoryId_Book { get; set; }
    }
}